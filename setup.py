#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
from os.path import join as pjoin
from glob import glob

from setuptools import setup, find_namespace_packages


WORKING_PATH = os.getcwd()


with open(pjoin("evoclearn", "opt", "version.py")) as infh:
    version_py = {}
    exec(infh.read(), version_py)
    package_name = version_py["package_name"]
    version = version_py["__version__"]


python_requires = ">=3.7"


install_requires = [
    "click",
    "nlopt==2.7.0",
    "hyperopt==0.2.5",
    "evoclearn-core>=0.18.6,<0.19.0",
    "evoclearn-rec>=0.5.3,<0.6.0"
]


extras_require = {
    "dev": ["ipython",
            "matplotlib"]
}


setup_args = {
    "name": package_name,
    "version": version,
    "description": "Tools for Early Vocal Learning optimisation experiments.",
    "long_description": "Tools for Early Vocal Learning optimisation experiments...",
    "author": "The EVocLearn Group",
    "author_email": "daniel.r.vanniekerk@gmail.com",
    "url": "https://gitlab.com/evoc-learn-group/evoc-learn-opt",
    "packages": find_namespace_packages(include=["evoclearn.*"]),
    "python_requires": python_requires,
    "install_requires": install_requires,
    "extras_require": extras_require,
    "package_data": {"": ["*.json", "*.xml", "*.wav", "*.TextGrid"]},
    "entry_points": {
        "console_scripts": [
            "evl_opt=evoclearn.opt.cli:main",
        ]
    }
}

setup(**setup_args)
