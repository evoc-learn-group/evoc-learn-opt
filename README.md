Evoclearn-opt
=============

This Python package provides a tool for implementing goal-directed vocal exploration as an optimisation task. It can be used to find optimised articulatory parameters to produce syllables on the basis of acoustic matching or auditory perceptual goals defined as percept vectors (implemented in [evoclearn-rec](https://gitlab.com/evoc-learn-group/evoc-learn-rec)).

If you use this tool, please cite:

```
@inproceedings{vanniekerk22_optccv,
  title = {{Exploration strategies for articulatory synthesis of complex syllable onsets}},
  author = {Van Niekerk, D. R. and Xu, A. and Gerazov, B. and Krug, P. K. and Birkholz, B. and Xu, Y.},
  booktitle = {{Proc. Interspeech}},
  year = {2022},
  month = sep,
  address = {Incheon, South Korea},
  pages = {635--639}
}
```


## Getting started

#### Installation

Install the latest release version from the *Python Package Index* with:

```bash
pip install evoclearn-opt
```

Or build a package from this source repository and install with:

```bash
python setup.py bdist_wheel
pip install dist/evoclearn_opt-0.19.6-py3-none-any.whl
```

You will need Python [setuptools](https://pypi.org/project/setuptools/), [wheel](https://pypi.org/project/wheel/).


#### Quickstart

Evoclearn-opt implements a range of experimental conditions and optional inputs and outputs, however, a good way to get started is via the following Python notebook which provides an example of running the simulation to find a pre-specified CV syllable:
 - [Articulatory exploration with Evoclearn-opt -- Discovering CV syllables](https://www.tinyurl.com/evoclearn)
 
The example notebook is also described in the following presentation at time 10m34s:
 - [Evoclearn Workshop (Macedonia) -- Learning CVs and CCVs](https://youtu.be/WnaiGEyBu8c?t=634)
