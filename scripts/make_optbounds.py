import json
import os

from evoclearn.core import io

from evl_opt.opt.utils import STD_VARPARAMS, C_VARPARAMS
from evl_opt.opt import ETC_DIR


for speaker in ["JD2", "boy3"]:
    optbounds = {}
    with open(os.path.join(ETC_DIR, "speakers", speaker, f"speaker.bounds.json")) as infh:
        speakerbounds = io.load_bounds(infh)

    # Unconstrained:
    optbounds["Vstd"] = {"V-"+k: (v["min"], v["max"])
                         for k, v in speakerbounds.items()
                         if k in STD_VARPARAMS[speaker]}
    optbounds["Cstd"] = {"C-"+k: (v["min"], v["max"])
                         for k, v in speakerbounds.items()
                         if k in STD_VARPARAMS[speaker]}
    optbounds["Vstd_Cstd"] = {**optbounds["Vstd"], **optbounds["Cstd"]}

    # Constrained C:
    for consonant in C_VARPARAMS:
        c_optbounds = {}
        for k, v in speakerbounds.items():
            if k in C_VARPARAMS[consonant]:
                c_optbounds[f"C-{k}"] = (v["min"], v["max"])
        optbounds[f"C{consonant}"] = c_optbounds

        optbounds[f"Vstd_C{consonant}"] = {**optbounds["Vstd"], **optbounds[f"C{consonant}"]}

    # To files:
    for k, v in optbounds.items():
        with open(os.path.join(ETC_DIR, "speakers", speaker, f"{k}.bounds.json"), "w") as outfh:
            json.dump(v, outfh, indent=2)
